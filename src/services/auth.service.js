import jwtDecode from 'jwt-decode';
import axios from '../services/axios.service';

class AuthService {
  setAxiosInterceptors = (propx) => {
    const { onLogout,onRefreshToken, refresh_token } = propx;
    const time = `${Date.now()}`.substr(9);

    // Default Axios
    axios.defaults.baseURL = process.env.REACT_APP_API_URL_NEST;
    axios.defaults.timeout = 30000; // 30sec
    axios.defaults.headers.Accept = '*/*';
    axios.defaults.headers['Content-Type'] = 'application/json';
    // axios.defaults.withCredentials = false; // ถ้าใส่ true  แล้วจะติด Access-Control-Allow-Origin

    // On request
    axios.interceptors.request.use((request) => request);

    // On response
    axios.interceptors.response.use(
      (response) => response,
      async(error) => {
        let originalConfig = error.config
       
        console.error(`RES:${time}`, { status: error?.response?.status || 503, error: `${error}` });
        if (error.response && error.response.status === 401 && !originalConfig._retry) {
          originalConfig._retry = true;
          // Success Refresh Token with axios
          try{
            const access_token  = await axios.post(`${process.env.REACT_APP_API_HOST}/refresh-token/`, {
              refresh_token: refresh_token
            }).then((response) => {
              onRefreshToken(response.data)
              return response.data.access_token
            }).catch((err) => err)
             
             originalConfig.headers.Authorization = `Bearer ${access_token}`
             
              return axios(originalConfig);
          }catch(_error) {
            if (_error.response && _error.response?.data) {
              return Promise.reject(_error.response?.data);
            }
            return Promise.reject(_error);
          }
          //end Success Refresh Token with Axios
          
        }
        if(error.response && error.response.status === 406) {
          if (onLogout) {
            // alert(`onLogout - ${error}`);
            onLogout();
          }
          return Promise.reject(error?.response?.data);
        }
        
        return Promise.reject(error);
      },
    );
  };

  // กำหนด token ที่ axios
  setAuthorization = (accessToken) => {
    axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;
    console.log(axios.defaults.headers)
  };
  // ลบ token ออกจาก axios
  delAuthorization = () => {
    delete axios.defaults.headers.common.Authorization;
  };

  // ตรวจสอบ token ว่ามีอยู่จริงและยังไม่หมดอายุ
  isValidToken = (token) => {
    if (!token) {
      return false;
    }
    return true
    // const decoded: any = jwtDecode(token);
    // const currentTime = Date.now() / 1000;
    // return decoded.exp > currentTime;
  };
}

const authService = new AuthService();

export default authService;
