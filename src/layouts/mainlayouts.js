
import Header from './header'
import Footer from './footer'
import Container from '@mui/material/Container';
export default function Layout(props) {
  return (
    <div>
      {props.Header ? <Header /> : null}
      <Container maxWidth="lg" sx={props.style ? props.style : null}>
        {props.children}
      </Container>
      {props.Header ? <Footer /> : null}
    </div>
  );
}