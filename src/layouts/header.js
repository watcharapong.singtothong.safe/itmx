import * as React from 'react';
import { styled, alpha } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import { Link } from "react-router-dom";
import Container from '@mui/material/Container';



const Menubar = styled('div')(() => ({
  cursor: 'pointer',
  color: 'black',
  display: 'inline-flex',
  padding: '10px 10px',
  margin: '10px 10px',
  borderBottom: '2px solid white ',
  ':hover': {
    borderBottom: '2px solid black',

  },
}));

export default function SearchAppBar() {

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" style={{ backgroundColor: 'white', padding: '0px 0px' }}>
        <Toolbar>
          <Container>
            <div style={{ display: 'flex',justifyContent:'space-between' }}>
              <img
                src={``}
                alt={`arter`}
                loading="lazy"
                style={{padding: '10px', maxHeight:'60px'}}
              />
              <div>
              <Link to={`/`} style={{ textDecoration:'none'}}>
                <Menubar>
                  HOME
                </Menubar></Link>
                <Link to={`/Products`} style={{ textDecoration:'none'}} >
                <Menubar>
                  Products
                </Menubar></Link>
              </div>
            </div>
          </Container>
        </Toolbar>

      </AppBar>
    </Box>
  );
}