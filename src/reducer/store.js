import { combineReducers } from 'redux'
import products from './products/productsReuder';

const rootReducer = combineReducers({
  products,
})
export default rootReducer