
const initialLanguage =
{
    product : [],
};
const language = (state = initialLanguage, action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            var tempdata = [...state.product]
            tempdata.push({...action.product})
             state.product = tempdata
             return {...state}            
        case 'DEL_PRODUCT':
            var tempdata = [...state.product]
            tempdata.splice( action.product, 1)
            state.product = tempdata
            return {...state}
        case 'EDIT_PRODUCT':
            var tempdata = [...state.product]
            tempdata[action.id] = action.product
            state.product = tempdata
            return {...state}
        default:
            return state
    }
}

export default language