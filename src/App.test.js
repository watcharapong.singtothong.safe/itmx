import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store, persistor } from './reducer/configureStore';
import App from './App';
import './index.css';
import { BrowserRouter } from "react-router-dom";
import { AuthProvider } from "./auth/useAuth";
import { PersistGate } from 'redux-persist/integration/react'

test('renders learn react link', () => {
  const { getByText } = render(
    <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <AuthProvider>
          <App />
        </AuthProvider>
      </BrowserRouter>
      </PersistGate>
    </Provider>
  );

  expect(getByText(/learn/i)).toBeInTheDocument();
});
