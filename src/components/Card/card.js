
import React from 'react';
import { Link } from "react-router-dom";
export function Boxdetail(props) {
  return (<>
    <div className="card" style={{ border: '1px solid black', borderRadius: '20px'}}>
      <div style={{ display: 'flex', justifyContent: 'flex-end',padding:'10px' }}> <div style={{marginRight:'10px',cursor:'pointer'}}onClick={props.onedit} >editicon </div> <div style={{cursor:'pointer'}} onClick={props.ondel}> deleteicon</div> </div>
      <div className="card" style={{ textAlign: 'center', padding: '20px', display: 'flex' }}>
        <div>
          <img src={props.data.img} alt="" style={{ width: '300px', height: '300px' }} />
          <h3>Price : {props.data.price || 'No detail'}</h3>
        </div>
        <div style={{ marginLeft: '20px', width: '100%', textAlign: 'left' }}>
          <p>Name : {props.data.name || 'No detail'}</p>
          <p>Detail : {props.data.detail || 'No detail'}</p>
        </div>
      </div>
    </div></>
  );
};

