import * as React from "react";
import { Routes, Route , useParams} from "react-router-dom";

import Home from "../pages/Homepage/homepage"
import Products from "../pages/Products/product"
import Notfound from "../pages/Notfound/notfound"


function Linkroute() {
  return (
    <div>
      <Routes>
        <Route index element={<Home />} />
        <Route path="/products" element={
          <Products />
        } />
        <Route path="*" element={<Notfound />} />
      </Routes>
    </div>
  );
}

export default Linkroute;