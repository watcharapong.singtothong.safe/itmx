
import { Link } from "react-router-dom";
import Layout from '../../layouts/mainlayouts';

export default function Notfound() {
    return (
      <Layout
      style={
        {
          background: 'linear-gradient(to right, #4880EC, #019CAD)',
          height: '100vh',
          padding: 0,
          maxWidth: '100% !important',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }
      }>
      <div>
        <h2 >Nothing to see here! 404</h2>
        <p>
          <Link to="/">Go to the home page</Link>
        </p>
      </div>
      </Layout>
    );
  }