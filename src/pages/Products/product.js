import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import Layout from '../../layouts/mainlayouts';
import { Boxdetail } from '../../components/Card/card';
import TextField from '@mui/material/TextField';
import { useParams } from 'react-router-dom';
import TextareaAutosize from '@mui/base/TextareaAutosize';
import { Modal, Box, Button, Grid, Skeleton } from '@mui/material';

const initData = {
  'name': '',
  'detail': '',
  'price': '',
  'img': '',
}
export default function Product(props) {
  const dispatch = useDispatch();
  const product = useSelector((state) => state.products.product)
  const [seleted, setSeleted] = useState('')
  const [tigger, setTigger] = useState(true)
  const [modalcreate, setmodalcreate] = useState(false)
  const [modaledit, setmodaledit] = useState(false)
  const [tempdata, setTempdata] = useState(initData)
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '50%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    m: 5,
  };

  const setProduct = () => {
    dispatch({ type: 'ADD_PRODUCT', product: tempdata })
    setmodalcreate(false)
  }
  const delProduct = (id) => {
    dispatch({ type: 'DEL_PRODUCT', product: id  })

  }
  const editProduct = () => {
    dispatch({ type: 'EDIT_PRODUCT', product: tempdata , id:seleted })
    setmodaledit(false)

  }
  const modalEditProduct = (id) => {
    setSeleted(id);
    setTempdata(product[id])
    setmodaledit(true)
  }
  const hendleInputchnage = (e) => {
    var tempCange = {...tempdata}
    tempCange[e.target.name] = e.target.value
    setTempdata(tempCange)
    setTigger(!tigger)
  }
  const delimgchnage = (e) => {
    var tempCange = {...tempdata}
    tempCange['img'] = ''
    setTempdata(tempCange)
    setTigger(!tigger)
  }
  const hendleSave = () => {
    setProduct()
  }
  const handlefileupload = (event, i) => {
    if (event.target.files.length > 0) {
      var type_png = "image/png"
      var type_jpg = "image/jpeg"
      var fixsize = '100'
      if (process.env.REACT_APP_SIZE_IMG !== undefined) {
        fixsize = process.env.REACT_APP_SIZE_IMG;
      }
      if (event.target.files[0].type === type_png || event.target.files[0].type === type_jpg) {
        if ((parseInt(fixsize) * 1000000) > event.target.files[0].size) {
          let img = event.target.files[0]
          var tempCange = tempdata
          const localImageUrl =  window.URL.createObjectURL(img);
          tempCange['img'] = localImageUrl

          setTempdata(tempCange)
          setTigger(!tigger)
        } else {
          alert(`ขนาดรูปภาพเกิน ${fixsize} MB`)
        }
      } else {
        alert(`ขนาดรูปภาพไม่ใช่ประเภท .jpg .jpeg .png`)
      }
    }

  }
  return (
    <Layout Header={true}>
      <Skeleton variant="rectangular" width="100%" animation="wave" style={{ height: '500px',p:1 }} />
      <div style={{ padding:3 }}>
      <Button variant="contained" onClick={() => setmodalcreate(true)}>Create Product</Button>
      </div>
      <Grid container spacing={4}>
        {product.length > 0 && product.map((item,index) => {
          return <Grid item xs={6}><Boxdetail data={item} ondel={()=>delProduct(index)} onedit={()=>modalEditProduct(index)} /></Grid>
        })}
      </Grid>
      <Modal
        open={modalcreate}
        onClose={() => setmodalcreate(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div style={{ padding: '20px' }}>
            <h1>Create Product</h1>
            <div style={{ display: 'flex', alignItems: 'baseline', padding: '10px' }}>
              <Grid container spacing={4}>
                <Grid item xs={12}>
                  Image :
                  <input
                    type="file"
                    accept="image/*"
                    name="img"
                    style={{ padding: '0px 10px' }}
                    onChange={(event) => handlefileupload(event, 0)}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField id="outlined-basic" label="Name" variant="outlined" size="small" name="name" onChange={hendleInputchnage} value={tempdata.name} />
                </Grid>
                <Grid item xs={6}>
                  <TextField id="outlined-basic" label="Price" variant="outlined" size="small" name="price" onChange={hendleInputchnage} value={tempdata.price} />
                </Grid>
                <Grid item xs={12}>
                  <TextareaAutosize
                    aria-label="Detail"
                    placeholder="Detail"
                    name="detail"
                    style={{ width: '100%', border: '1px solid rgba(0, 0, 0, 0.23)' }}
                    onChange={hendleInputchnage}
                    minRows={3}
                    value={tempdata.detail}
                  />
                </Grid>
              </Grid>
            </div>
          </div>
          <button onClick={hendleSave}> save </button>
        </Box>
      </Modal>
      <Modal
        open={modaledit}
        onClose={() => setmodaledit(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div style={{ padding: '20px' }}>
            <h1>Edit Product</h1>
            <div style={{ display: 'flex', alignItems: 'baseline', padding: '10px' }}>
              <Grid container spacing={4}>
                <Grid item xs={12} >
                 {  !tempdata.img ?
                  <input
                    type="file"
                    accept="image/*"
                    name="img"
                    id="actual-btn"
                    style={{ padding: '0px 10px' }}
                    onChange={(event) => handlefileupload(event, 0)}
                  />: 
                  <div style={{position:'relative',width: '300px', height: '300px'}}>
                    <div style={{position:'absolute' , color:'red',fontSize:'20px',right:'10px'}} onClick={()=>{delimgchnage()}}>x</div>
                  <img src={tempdata.img} alt="" style={{ width: '300px', height: '300px' }} for='img' />
                  </div>
                  }
                </Grid>
                <Grid item xs={6}>
                  <TextField id="outlined-basic" label="Name" variant="outlined" size="small" name="name" onChange={hendleInputchnage} value={tempdata.name} />
                </Grid>
                <Grid item xs={6}>
                  <TextField id="outlined-basic" label="Price" variant="outlined" size="small" name="price" onChange={hendleInputchnage} value={tempdata.price} />
                </Grid>
                <Grid item xs={12}>
                  <TextareaAutosize
                    aria-label="Detail"
                    placeholder="Detail"
                    name="detail"
                    style={{ width: '100%', border: '1px solid rgba(0, 0, 0, 0.23)' }}
                    onChange={hendleInputchnage}
                    minRows={3}
                    value={tempdata.detail}
                  />
                </Grid>
              </Grid>
            </div>
          </div>
          <button onClick={editProduct}> save </button>
        </Box>
      </Modal>
    </Layout>
  );
}
