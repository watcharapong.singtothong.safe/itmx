import React, { useEffect } from 'react';
import { CardMenu } from "../../components/Card/card";
//json-data
import menuData from '../../data/menu.json';
import Layout from '../../layouts/mainlayouts';
import Box from '@mui/material/Box';
import Skeleton from '@mui/material/Skeleton';
export default function Home() {

  
  useEffect(() => {
    console.log(menuData)
  }, []);


  const App = () => {
    return (
      <Box sx={{ width: "100%" }}>
      <Skeleton  variant="rectangular" width="100%" animation="wave" style={{height:'500px'}} />
    </Box>
    );
  };

  return (
    <Layout Header={true}>
      <div style={{ padding: '10px' }}>
        <App />
      </div>
    </Layout>
  );
}