-- Answer #1
SELECT agents.* , SUM(orders.ORD_AMOUNT) as total
FROM agents 
LEFT JOIN orders on agents.AGENT_CODE = orders.AGENT_CODE 
GROUP BY agents.AGENT_CODE
ORDER BY total DESC
LIMIT 1
-- Answer #2
SELECT customer.CUST_CODE, customer.CUST_NAME ,SUM(orders.ORD_AMOUNT) as total
FROM customer 
LEFT JOIN orders on customer.CUST_CODE = orders.CUST_CODE 
GROUP BY customer.CUST_CODE, customer.CUST_NAME
having  SUM(orders.ORD_AMOUNT) > '5000'
-- Answer #3
SELECT agents.AGENT_CODE , COUNT(agents.AGENT_CODE) as counts
FROM agents 
LEFT JOIN orders on agents.AGENT_CODE = orders.AGENT_CODE 
WHERE orders.ORD_DATE	 > '2008-7-1' and orders.ORD_DATE	 < '2008-7-31'
GROUP BY agents.AGENT_CODE DESC
ORDER BY counts DESC
